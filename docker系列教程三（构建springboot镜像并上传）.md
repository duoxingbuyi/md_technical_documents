### docker系列教程三（构建springboot镜像并上传）



#### springboot项目结构

![1597151539060](docker系列教程图片\1597151539060.png)



### 1.pom.xml配置

> 镜像名

```xml
<properties>
        <!--docker镜像名-->
        <docker.image.prefix>demo-service</docker.image.prefix>
        ...
</properties>
```

> 打包插件

```xml
<plugins>
    <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <configuration>
            <fork>true</fork>
        </configuration>
    </plugin>
    <!-- 跳过单元测试 -->
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
            <skipTests>true</skipTests>
        </configuration>
    </plugin>
    <!--docker插件-->
    <plugin>
        <groupId>com.spotify</groupId>
        <artifactId>docker-maven-plugin</artifactId>
        <version>1.0.0</version>
        <!--将插件绑定在某个phase执行-->
        <executions>
            <execution>
                <id>build-image</id>
                <!--绑定在package这个phase上。执行mvn package 自动执行mvn docker:build-->
                <phase>package</phase>
                <goals>
                    <goal>build</goal>
                </goals>
            </execution>
        </executions>
        <configuration>
            <imageName>${docker.image.prefix}/${project.artifactId}</imageName>
            <!--指定标签-->
            <imageTags>
                <imageTag>latest</imageTag>
            </imageTags>
            <!-- 指定 Dockerfile 路径-->
            <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
            <!--指定远程 docker api地址-->
            <dockerHost>http://192.168.122.1:2375</dockerHost>
            <resources>
                <resource>
                    <targetPath>/</targetPath>
                    <directory>${project.build.directory}</directory>
                    <include>${project.build.finalName}.jar</include>
                </resource>
            </resources>
        </configuration>
    </plugin>
</plugins>
```

#### 2.编写Dockerfile

```dockerfile
FROM java:8
EXPOSE 8080

VOLUME /tmp
ADD demo-service.jar /app.jar
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-jar","/app.jar"]

```

#### 3.编写docker-compose.yml

```yaml
version: '2'
services:
  demo-service:
    image: demo-service/demo-service
    ports:
      - "8080:8080"
    environment:
      - spring.profiles.active=dev
```

#### 4.构建镜像并上传

```
mvn clean package
```

#### 5.查看docker镜像是否上传成功

连接远程docker服务器，执行命令

```bash
docker images
```

![1597151804676](docker系列教程图片\1597151804676.png)

#### 6.启动镜像

> 使用docker-compose 启动镜像

```bash
# 切换到docker-compose.yml所在目录，执行命令
docker-compose up
```

> 可能出现的错误

```bash
ERROR: Failed to Setup IP tables: Unable to enable SKIP DNAT rule:  (iptables failed: iptables --wait -t nat -I DOCKER -i br-2add1a39bc5d -j RETURN: iptables: No chain/target/match by that name.
原因是关闭防火墙之后docker需要重启，执行以下命令重启docker即可：
```

```bash
systemctl restart docker
# 再次启动
docker-compose up
```

> 后台启动

```bash
docker-compose up -d
```

