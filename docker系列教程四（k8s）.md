### docker系列教程四（基于docker本地运行K8S）

#### 1.先决条件

> 1. 你必须拥有一台安装有Docker的机器。 
>
> 2. 你的内核必须支持 memory and swap accounting 。确认你的linux内核开启了如下配置： 

```bash
CONFIG_RESOURCE_COUNTERS=y
CONFIG_MEMCG=y
CONFIG_MEMCG_SWAP=y
CONFIG_MEMCG_SWAP_ENABLED=y
CONFIG_MEMCG_KMEM=y
```

> 3. 以命令行参数方式，在内核启动时开启 memory and swap accounting 选项： 

```bash
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1"
```

 注意：以上只适用于GRUB2。通过查看/proc/cmdline可以确认命令行参数是否已经成功 

 传给内核： 

```bash
$cat /proc/cmdline
BOOT_IMAGE=/boot/vmlinuz-3.18.4-aufs root=/dev/sda5 ro cgroup_enable=memory
swapaccount=1
```

#### 2.拉取镜像安装

```bash
docker pull mirrorgooglecontainers/etcd:2.0.12
docker tag docker.io/mirrorgooglecontainers/etcd:2.0.12 gcr.io/google_containers/etcd:2.0.12
docker pull mirrorgooglecontainers/hyperkube:v1.0.1
docker images
```

