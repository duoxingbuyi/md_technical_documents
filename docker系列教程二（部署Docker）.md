### docker系列教程二（部署Docker）

###### 1.设置镜像 

```bash
vim /etc/docker/daemon.json
```

 填写： 

```bash
{
  "registry-mirrors": ["http://hub-mirror.c.163.com"]
}
```

###### 2.开放管理端口映射 

> （1）修改/lib/systemd/system/docker.service文件 

```bash
vim /lib/systemd/system/docker.service
```

将第13行的ExecStart=/usr/bin/dockerd，替换为：

```bash
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock -H tcp://0.0.0.0:7654
```

2375是管理端口，7654是备用端口

> （2）在~/.bashrc中写入docker管理端口 

```bash
vim ~/.bashrc
```

添加内容

```bash
export DOCKER_HOST=tcp://0.0.0.0:2375
```

重载配置

```bash
source ~/.bashrc
```

执行

```bash
unset DOCKER_HOST
unset DOCKER_TLS_VERIFY
unset DOCKER_TLS_PATH
```

关闭防火墙或开启2375端口

######  3.重启docker 

```bash
systemctl daemon-reload
systemctl restart docker
```

###### 4. 测试docker是否正常安装和运行 

> 检查端口是否开启

```bash
netstat -tunlp | grep 2375
```

> 检查是否可以远程访问

打开本机cmd

```
telnet ip 端口
```

###### 5.安装docker-compose

>  下载 

```bash
curl -L https://get.daocloud.io/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

>  设置可运行 

```bash
chmod +x /usr/local/bin/docker-compose
```

>  版本检查 

```bash
docker-compose --version
```

