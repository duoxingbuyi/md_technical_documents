### docker系列教程一（安装启动）

###### CentOS7 Docker 安装

使用国内 daocloud 一键安装命令：

```bash
curl -sSL https://get.daocloud.io/docker | sh
```

###### 安装 Docker Engine-Community

>  安装所需的软件包 

```bash
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```

>  使用以下命令来设置稳定的仓库 (阿里云)

```bash
sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

>  安装最新版本的 Docker Engine-Community 和 containerd 

```bash
sudo yum install docker-ce docker-ce-cli containerd.io
```

######  启动 Docker 

```bash
sudo systemctl start docker
```

>  通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。 

```bash
sudo docker run hello-world
```

